package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class activity {

    public static void main(String[] args){

        int num1 = 0;
        try{
            Scanner input = new Scanner(System.in);
            System.out.println("Input an integer whose factorial will be computed");
            num1 = input.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Input is not a number");
        } catch (Exception e){
            System.out.println("Invalid Input");
        } finally {
            int i, fact = 1;
            for (i = 1; i <= num1; i++) {
                fact *= i;
            }
            System.out.println("Factorial of " + num1 + " is: " + fact);
        }
    }
}